
#pragma once

#include "EnemyShip.h"

class BioEnemyShip : public EnemyShip
{

public:

	BioEnemyShip();
	virtual ~BioEnemyShip() { }

	void SetTexture(Texture *pTexture) { m_pTexture = pTexture; } // loads image of enemy

	virtual void Update(const GameTime *pGameTime); // updates position of enemy

	virtual void Draw(SpriteBatch *pSpriteBatch); // displays enemy


private:

	Texture *m_pTexture;

};