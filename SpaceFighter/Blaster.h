
#pragma once

#include "Weapon.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0; // weapon cool down
		m_cooldownSeconds = .01;
	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime) //constantly checks for an update of the blaster
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed(); // checks to see if cooldown is greater than 0, if so 
	}

	virtual bool CanFire() const { return m_cooldown <= 0; } // can fire if cooldown is 0

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType) //blaster fires projectile
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile *pProjectile = GetProjectile(); // loads projectile image
				if (pProjectile)
				{
					pProjectile->Activate(GetPosition(), true); // blaster shoots projectile
					m_cooldown = m_cooldownSeconds; // adds cooldown
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};