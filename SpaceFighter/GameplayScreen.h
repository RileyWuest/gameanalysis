
#pragma once

#include "KatanaEngine.h"

using namespace KatanaEngine;

class Level;

class GameplayScreen : public Screen
{

public:

	GameplayScreen(const int levelIndex = 0);

	virtual ~GameplayScreen() { }

	virtual void LoadContent(ResourceManager *pResourceManager); //loads all of the content for the bootup of the game

	virtual void HandleInput(const InputState *pInput); // checks for input by user

	virtual void Update(const GameTime *pGameTime); // checks for any updates made 

	virtual void Draw(SpriteBatch *pSpriteBatch); // displays all characters


private:

	Level *m_pLevel;


};
